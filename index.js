
const FIRST_NAME = "Andra-Elisa";
const LAST_NAME = "Ceica";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    var cache = {
        home : 0,
        about : 0,
        contact : 0,
        pageAccessCounter : function(counter){
            if( typeof counter === 'undefined'){
                this.home++;
            }
            else {
                if(counter.toLowerCase() === 'about'){
                    this.about++;
                }
            
                else{
                    if(counter.toLowerCase() === 'contact'){
                        this.contact++;

                    }
                }
            }

        },
        getCache : function(){
            return this;
        }
        
    }
   
    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

